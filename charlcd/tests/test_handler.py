#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.handler"""

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from charlcd import buffered as lcd
from charlcd.drivers.null import Null
from charlcd.handler import Handler


class TestHandler(object):
    def setUp(self):
        self.screen = lcd.CharLCD(10, 2, Null())
        self.screen.init()
        self.handler = Handler(self.screen)
        self.message = {
            'protocol': 'iot:1',
            'node': 'computer',
            'chip_id': 'd45656b45afb58b1f0a46',
            'event': 'lcd.content',
            'parameters': {
                'content': [
                    '1234567890',
                    'abcdefghij'
                ]
            },
            'targets': [
                'ALL'
            ]
        }

    def test_init(self):
        assert_equal(self.screen, self.handler.worker)

    def test_has_handle_function(self):
        self.handler.handle(self.message)

    def test_handle_lcd_content(self):
        self.handler.handle(self.message)
        assert_equal(self.handler.worker.screen, self.message['parameters']['content'])




