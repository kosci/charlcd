#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from nose.tools import assert_raises
from charlcd.abstract import lcd_virtual
from charlcd import virtual_direct
from charlcd import direct
from charlcd.drivers.null import Null


class TestLcdVirtual(object):
    def test_init(self):
        screen = lcd_virtual.CharLCDVirtual(20, 6)
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 6)
        assert_equal(screen.get_display_mode(), 'direct')

    def test_init_buffered_mode(self):
        screen = lcd_virtual.CharLCDVirtual(20, 6, lcd_virtual.DISPLAY_MODE_BUFFERED)
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 6)
        assert_equal(screen.get_display_mode(), 'buffered')

    def test_add_one_lcd(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd = direct.CharLCD(20, 3, Null())
        screen.add_display(0, 0, lcd)
        lcds = []
        lcds.append({
            'x': 0,
            'y': 0,
            'offset_x': 0,
            'offset_y': 0,
            'width': 20,
            'height': 3,
            'lcd': lcd
        })
        assert_equal(screen.get_displays(), lcds)

    def test_add_two_lcds(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 5, lcd2)
        lcds = []
        lcds.append({
            'x': 0,
            'y': 0,
            'offset_x': 0,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        })
        lcds.append({
            'x': 2,
            'y': 5,
            'offset_x': 0,
            'offset_y': 0,
            'width': 16,
            'height': 2,
            'lcd': lcd2
        })
        assert_equal(screen.get_displays(), lcds)

    def test_get_display_one(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 5, lcd2)
        lcd1_object = {
            'x': 0,
            'y': 0,
            'offset_x': 0,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        }
        assert_equal(screen.get_display(0, 0), lcd1_object)
        assert_equal(screen.get_display(19, 3), lcd1_object)
        assert_equal(screen.get_display(10, 2), lcd1_object)

    def test_get_display_two(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 4, lcd2)
        lcd2_object = {
            'x': 2,
            'y': 4,
            'offset_x': 0,
            'offset_y': 0,
            'width': 16,
            'height': 2,
            'lcd': lcd2
        }
        assert_equal(screen.get_display(2, 4), lcd2_object)
        assert_equal(screen.get_display(17, 5), lcd2_object)

    def test_get_display_none(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 4, lcd2)
        assert_equal(screen.get_display(1, 4), None)
        assert_equal(screen.get_display(19, 5), None)

    def test_get_display_error(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 4, lcd2)
        assert_raises(IndexError, screen.get_display, 40, 0)

    def test_get_display_with_offset(self):
        screen = virtual_direct.CharLCD(16, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1, 4, 0)
        screen.add_display(0, 4, lcd2)
        lcd1_object = {
            'x': 0,
            'y': 0,
            'offset_x': 4,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        }
        lcd2_object = {
            'x': 0,
            'y': 4,
            'offset_x': 0,
            'offset_y': 0,
            'width': 16,
            'height': 2,
            'lcd': lcd2
        }
        assert_equal(screen.get_display(0, 0), lcd1_object)
        assert_equal(screen.get_display(15, 5), lcd2_object)

    def test_get_display_should_use_offset(self):
        screen_1 = virtual_direct.CharLCD(16, 6)
        screen_2 = virtual_direct.CharLCD(4, 4)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen_1.add_display(0, 0, lcd2)
        screen_1.add_display(0, 2, lcd1, 4, 0)

        screen_2.add_display(0, 0, lcd1)
        screen_1_lcd1_object = {
            'x': 0,
            'y': 2,
            'offset_x': 4,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        }
        screen_1_lcd2_object = {
            'x': 0,
            'y': 0,
            'offset_x': 0,
            'offset_y': 0,
            'width': 16,
            'height': 2,
            'lcd': lcd2
        }
        screen_2_lcd1_object = {
            'x': 0,
            'y': 0,
            'offset_x': 0,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        }
        assert_equal(screen_1.get_display(0,0), screen_1_lcd2_object)
        assert_equal(screen_2.get_display(0,0), screen_2_lcd1_object)

    def test_get_display_should_use_offset_return_correct_vertical(self):
        screen_1 = virtual_direct.CharLCD(10, 2)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())

        screen_1.add_display(0, 0, lcd2, 11, 0)
        screen_1.add_display(6, 0, lcd1, 4, 0)

        screen_1_lcd1_object = {
            'x': 6,
            'y': 0,
            'offset_x': 4,
            'offset_y': 0,
            'width': 20,
            'height': 4,
            'lcd': lcd1
        }
        screen_1_lcd2_object = {
            'x': 0,
            'y': 0,
            'offset_x': 11,
            'offset_y': 0,
            'width': 16,
            'height': 2,
            'lcd': lcd2
        }

        assert_equal(screen_1.get_display(0,0), screen_1_lcd2_object)
        assert_equal(screen_1.get_display(6,0), screen_1_lcd1_object)
        assert_equal(screen_1.get_display(5,0), None)

