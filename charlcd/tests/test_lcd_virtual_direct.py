#!/usr/bin/python
# -*- coding: utf-8 -*-
#pylint: skip-file

"""Tests for charlcd.lcd"""

__author__ = 'Bartosz Kościów'

from nose.tools import assert_equal
from charlcd.drivers.null import Null
from charlcd import virtual_direct
from charlcd import direct


class TestLcdVirtualDirect(object):
    def init_screen(self):
        screen = virtual_direct.CharLCD(20, 6)
        lcd1 = direct.CharLCD(20, 4, Null())
        lcd2 = direct.CharLCD(16, 2, Null())
        screen.add_display(0, 0, lcd1)
        screen.add_display(2, 4, lcd2)
        return screen

    def test_init(self):
        screen = virtual_direct.CharLCD(20, 6)
        assert_equal(screen.get_width(), 20)
        assert_equal(screen.get_height(), 6)
        assert_equal(screen.get_display_mode(), 'direct')

    def test_string(self):
        screen = self.init_screen()
        screen.write('test me')
        assert_equal(screen.current_pos['x'], 7)
        assert_equal(screen.current_pos['y'], 0)

    def test_string_out_of_width(self):
        screen = self.init_screen()
        screen.write('test me 1 2 3 4 5 6 7 8 9 q w e r t y u i o p')
        assert_equal(screen.current_pos['x'], 20)
        assert_equal(screen.current_pos['y'], 0)

    def test_position_xy(self):
        screen = self.init_screen()
        screen.set_xy(0, 1)
        assert_equal(screen.current_pos['x'], 0)
        assert_equal(screen.current_pos['y'], 1)

    def test_position_xy_2nd(self):
        screen = self.init_screen()
        screen.set_xy(2, 5)
        lcd = screen.get_display(2, 5)
        screen.write('A')
        assert_equal(screen.current_pos['x'], 3)
        assert_equal(screen.current_pos['y'], 5)
        assert_equal(screen.active_display, lcd)

    def test_string_stream(self):
        screen = self.init_screen()
        screen.set_xy(11, 3)
        screen.stream("two liner and screener")
        assert_equal(screen.current_pos['x'], 13)
        assert_equal(screen.current_pos['y'], 4)
