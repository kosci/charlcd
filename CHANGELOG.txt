0.7.1
    - fix cmd in 8bit mode
0.7
    - add sdriver for popular i2c lcd module (i2cm.py)
0.6
    - flush(True) for buffered will rewrite all chars
    - shutdown() in GPIO drv will reset only used pins
    - add_custom_char(pos, bytes) add custom chars
0.5.1
    - move line addresses to driver
0.5.0
    - handler for: lcd.char, lcd.cmd, lcd.content events
0.4.0
    - WiFi content driver
    - interface flush_event_interface
    - events: pre_flush and post_flush
0.3.0
    - WiFi direct driver
0.2.3
    - small refactors
0.2.2
    - change PyPi name to lowercase
